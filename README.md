# NgMarvel

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.
This is a little web app with Angular 5, component oriented, this application has been modularized in components for its abstraction and reuse, considering the style guide provided by the angular documentation [Style 01-01](https://angular.io/guide/styleguide#style-01-01)

The application programming interface used was the provided by [Marvel](https://developer.marvel.com/), this way you get the data and manipulate it in the different sections of this simple page application (SPA).

Within the application you will be able to see the comics related to each one of the Marvel characters and save your favorites, these are stored within your browser's local storage and you can delete it when you want.

You also have a search field to be able to search for your character.

I hope you enjoy this small app :rocket:


## Demo

[http://ng-marvel-stage.surge.sh/](http://ng-marvel-stage.surge.sh/)


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
