import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class GlobalService {

  public apiUrl = 'http://gateway.marvel.com/';
  public apiVersion = 'v1/public/';
  public returnCharacters = 'characters';
  public returnComics = 'comics';
  public logoImg = '../../assets/icons/logo.png';
  public iconCharacter = '../../assets/icons/characters.png';
  public iconFavourites = '../../assets/icons/favourites.png';
  public apiParams = {
    'limit': 10,
    'ts': '12345',
    'apikey': 'f70b0ddb0a0b12b428864092013b23f0',
    'hash': '809a409d74bdb37f2dc267dff2d80ebc'
  };
  public params = new HttpParams()
    .set('limit', `${this.apiParams.limit}`)
    .set('ts', `${this.apiParams.ts}`)
    .set('apikey', `${this.apiParams.apikey}`)
    .set('hash', `${this.apiParams.hash}`);
  fav: any = [];
  searchText = new Subject<any>();
  constructor(public spinnerService: Ng4LoadingSpinnerService) { }

  saveFavouriteComic(comic) {
    let b = {};
    b = comic;
    this.fav = this.getFavouriteComic();
    if (this.fav !== null) {
      const x = this.fav.filter((comicid) => comicid.id === comic.id)[0];
      if (x) {
        alert('This comic is already in favorites');
      } else {
        this.fav.push(b);
        return localStorage.setItem('favComic', JSON.stringify(this.fav));
      }
    } else {
      this.fav = [];
      this.fav.push(b);
      return localStorage.setItem('favComic', JSON.stringify(this.fav));
    }
  }
  getFavouriteComic() {
    return JSON.parse(localStorage.getItem('favComic'));
  }
  updateFavourite(items) {
    return localStorage.setItem('favComic', JSON.stringify(items));
  }
  searchCharacter(search) {
    return this.searchText.next(search);
  }
  getData(): Observable<any> {
    return this.searchText.asObservable();
  }
  verifyFavComic(comic) {
    this.fav = this.getFavouriteComic();
    if (this.fav !== null) {
      const x = this.fav.filter((comicid) => comicid.id === comic.id)[0];
      return x;
    }
  }
}
