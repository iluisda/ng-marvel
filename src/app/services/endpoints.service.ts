import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EndpointsService {

  constructor(
    public globalService: GlobalService,
    public http: HttpClient) { }

  getCharaters() {
    const url = `${this.globalService.apiUrl}${this.globalService.apiVersion}${this.globalService.returnCharacters}`;
    const params = this.globalService.params;
    const resource = this.http.get(url, { params });
    return resource;
  }

  getComic(resourceURI) {
    const url = `${resourceURI}`;
    const params = this.globalService.params;
    const resource = this.http.get(url, { params });
    return resource;
  }
}
