import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRouting } from './app.routing';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
// BOOTSTRAP NGB COMPONENTS

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

import { SharedModule } from './shared/shared.module';
import { GlobalService } from './services/global.service';
import { EndpointsService } from './services/endpoints.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    SharedModule,
    FormsModule
  ],
  providers: [GlobalService, EndpointsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
