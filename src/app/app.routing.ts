import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  // {
  //   path: '',
  //   loadChildren: 'app/pages/home/home.module#HomeModule' //LAZY LOAD
  // },
  {
    path: '',
    loadChildren: 'app/pages/hero/hero.module#HeroModule', //LAZY LOAD
    data: { preload: true }
  },
  { path: '**', redirectTo: '/' }
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
