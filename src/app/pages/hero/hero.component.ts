import { Component, OnInit, Input, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { trigger, style, transition, animate } from '@angular/animations';
import { GlobalService } from '../../services/global.service';
import { EndpointsService } from '../../services/endpoints.service';

import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../../shared/shared.module';

@Component({
  selector: 'ngbd-modal-content',
  encapsulation: ViewEncapsulation.None,
  templateUrl: '../../shared/components/custom-modal/custom-modal.component.html',
  styleUrls: ['../../shared/components/custom-modal/custom-modal.component.scss']
})
export class NgbdModalContent {
  @Input() comic;
  @Input() verify;
  @Output() clickevent = new EventEmitter<string>();
  constructor(public activeModal: NgbActiveModal, public globalService: GlobalService) { }
  testclick(comic: string) {
    this.globalService.spinnerService.show();
    this.clickevent.emit(comic);
    setTimeout(() => {
      this.globalService.spinnerService.hide();
    }, 2);
    this.activeModal.close();
  }
}
@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss'],
  animations: [
    trigger('characters', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.5s ease-in', style({ opacity: '1' })),
      ])
    ])
  ]
})
export class HeroComponent implements OnInit {
  characters = [];
  comics = [];
  title = 'Characters';
  iconCharacter = this.globalService.iconCharacter;
  iconFavourites = this.globalService.iconFavourites;
  searchText: any;
  verify: any;
  constructor(
    public globalService: GlobalService,
    public endpointsService: EndpointsService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.globalService.getData().subscribe( x => {
      this.searchText = x;
    });
    this.globalService.spinnerService.show();
    this.endpointsService.getCharaters()
      .subscribe(result => {
        this.globalService.spinnerService.hide();
        this.characters = result['data']['results'];
        this.globalService.fav = this.globalService.getFavouriteComic();
      },
      err => {
        this.globalService.spinnerService.hide();
        console.log('Something went wrong!');
      });
  }

  getInfoComic($event) {
    this.globalService.spinnerService.show();
    this.endpointsService.getComic($event)
      .subscribe(result => {
          const modalRef = this.modalService.open(NgbdModalContent, {
            size: 'lg'
          });
          modalRef.componentInstance.comic = result['data']['results'];
          modalRef.componentInstance.verify = this.globalService.verifyFavComic(modalRef.componentInstance.comic['0']);
          this.globalService.spinnerService.hide();
          modalRef.componentInstance.clickevent.subscribe((comic) => {
            this.globalService.saveFavouriteComic(comic);
          });
      },
      err => {
        this.globalService.spinnerService.hide();
        console.log('Something went wrong!');
      });
  }

  deleteFavourite(array, id, element) {
    for (let i = 0; i < array.length; i++) {
      if (array[i] === array[element]) {
        array.splice(i, 1);
        this.globalService.updateFavourite(array);
      }
    }
  }
}
