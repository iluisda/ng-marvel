import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeroComponent } from './hero.component';

const routes: Routes = [
  {
    path: '',
    component: HeroComponent
  }
];

export const HeroRouting: ModuleWithProviders = RouterModule.forChild(routes);
