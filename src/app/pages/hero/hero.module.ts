import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { HeroRouting } from './hero.routing';
import { HeroComponent, NgbdModalContent } from './hero.component';
// import { HeroService } from './shared/hero.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HeroRouting
  ],
  providers: [
    // HeroService
  ],
  declarations: [HeroComponent, NgbdModalContent],
  entryComponents: [NgbdModalContent]
})
export class HeroModule { }
