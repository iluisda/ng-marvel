import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TruncateModule } from 'ng2-truncate';
import { CustomListComponent } from './components/custom-list/custom-list.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { WishListComponent } from './components/wish-list/wish-list.component';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from './pipes/filter.pipe';
// import { CustomModalComponent } from './components/custom-modal/custom-modal.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TruncateModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  exports: [
    CommonModule,
    CustomListComponent,
    NavbarComponent,
    WishListComponent,
    FilterPipe
  ],
  declarations: [CustomListComponent, NavbarComponent, WishListComponent, FilterPipe],
  entryComponents: []
})
export class SharedModule { }
