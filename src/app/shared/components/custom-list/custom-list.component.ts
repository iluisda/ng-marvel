import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'custom-list',
  templateUrl: './custom-list.component.html',
  styleUrls: ['./custom-list.component.scss']
})
export class CustomListComponent implements OnInit {
  @Input('name') characterName;
  @Input('description') characterDescription;
  // @Input('date') dateModified;
  @Input('thumbnail') thumbnailImage;
  @Input('comics') characterComics;
  @Output() comic = new EventEmitter<any>();

  name: string;
  description: string;
  // date: string;
  thumbnail: string;
  comics: any;

  constructor() { }

  ngOnInit() {
    this.name = this.characterName;
    this.description = this.characterDescription;
    // this.date = this.dateModified;
    this.thumbnail = this.thumbnailImage;
    this.comics = this.characterComics;
  }
  emitEvent(url) {
    this.comic.emit(url);
  }
}
