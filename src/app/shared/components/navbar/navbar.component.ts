import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  @Input('searchText') searchcomics: string;
  @Output() change = new EventEmitter<any>();

  searchText: string;
  constructor() { }

  public onSearchChange(searchText: any) {
    this.change.emit(this.searchText);
  }


}
