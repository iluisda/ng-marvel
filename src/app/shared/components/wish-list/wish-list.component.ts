import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.scss']
})
export class WishListComponent implements OnInit {
  @Input('name') favCharacterName;
  @Input('thumbnail') favThumbnailImage;
  @Output() deletecomic = new EventEmitter<any>();

  name: string;
  thumbnail: string;

  constructor() { }

  ngOnInit() {
    this.name = this.favCharacterName;
    this.thumbnail = this.favThumbnailImage;
  }

  emitEvent(favourite) {
    this.deletecomic.emit(favourite);
  }
}
